function [pred_train,pred_val,Bcoeffs] = LassoProc(Xtrain,Ytrain,Xval,folds,plots)
% This function takes training data and labels and perform
% lasso-regularized logistic regression using cross-validation.
% 
% The function returns the predicted labels of the training as well as the
% validation set, and the obtained coefficents.
%
% 
% Created by Julie A. E. Christensen.

% ======= USE IF TRAINING A LOG.REG. MODEL ===========================
% Train lassoglm model - "folds"-fold in lassoglm
[B, FitInfoTR] = lassoglm(Xtrain,Ytrain,'binomial','CV',folds,'Options',statset('UseParallel',true));

if strcmp(plots,'yes')
    % Plots of regularization
    lassoPlot(B,FitInfoTR,'PlotType','CV');
    lassoPlot(B,FitInfoTR,'PlotType','Lambda','XScale','log');
end

% indx = 75;
indx = FitInfoTR.Index1SE;
B0 = B(:,indx);

% Check for null model, if true select model with largest AUC value on
% training set
if all(B0)
    cnst = FitInfoTR.Intercept(indx);
    B1 = [cnst ; B0];
else
    for ii = 1:length(FitInfoTR.Lambda)
        indx = ii;
        B0 = B(:,indx);
        cnst = FitInfoTR.Intercept(indx);
        B1 = [cnst ; B0];
        pred_train = glmval(B1,Xtrain,'logit');
        [~,~,~,AUC(ii)] = perfcurve(Ytrain,pred_train,1);
        [~,indx] = max(AUC);
        B0 = B(:,indx);
        cnst = FitInfoTR.Intercept(indx);
        B1 = [cnst ; B0];
    end
end

pred_train = glmval(B1,Xtrain,'logit');
pred_val = glmval(B1,Xval,'logit');

Bcoeffs=B1;
