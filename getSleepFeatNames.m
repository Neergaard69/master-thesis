function names = getSleepFeatNames()
% Returns cell with names of extracted features.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

feats = {'MaxPAV','MinPAV','Mean','Variance','Skewness','Kurtosis','Mobility',...
         'Complexity','pDelta','pTheta','pAlpha','pBeta','pRelDelta',...
         'pRelTheta','pRelAlpha','pRelBeta','SpectralEdge','shEn','Hurst','NE'};
imfLabel = {'IMF1','IMF2','IMF3','IMF4','IMF5','IMF6','IMF7','IMF8','IMF9','IMF10'};
featNames = strcat(repmat(feats',length(imfLabel),1),' - ',reshape(repmat(imfLabel,length(feats),1),prod([length(feats) length(imfLabel)]),[]));

names = [featNames(:);{'Corr'}];

end