function names = getPSGfeatNames()
% Returns cell of feature names for narcolepsy model.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

names = {'NREM->NREM','NREM->REM','NREM->WAKE','REM->NREM','REM->REM',...
         'REM->WAKE','WAKE->NREM','WAKE->REM','WAKE->WAKE','SLEEP<->WAKE',...
         'TST','SL','RL','WASO','pN3','pN2','pN1','pREM','pW','Shannon Entropy'};
end