function suite = extractFeatureSuite(x,fs)
% This function takes an input time series or columns of time series and
% performs a feature extraction of 20 features distributed across three
% feature domains. 
%
% INPUT:    x       NxM matrix of M time series of length N each.
%           fs      Sampling frequency of time series.
%
% OUTPUT:   suite   1xM matrix of features
%
% Copyright:
% Alexander Neergaard Olesen, Technical University of Denmark, 2016

% Get dimensions
[~,M] = size(x);

% Time domain features
% -------------------------------------------------------------------------
mpav = [max(x); min(x)];                                                  

% Extract statistical parameters
mu      = nanmean(x);
sigma2  = var(x);
skew    = skewness(x);
kurt    = kurtosis(x);

% Extract Hjorth parameters
[mob,compl] = HjorthParameters(x);
% -------------------------------------------------------------------------


% Spectral features
% -------------------------------------------------------------------------

% Total power in signal
pTot = bandpower(x);

% Bandpower 
pDelta  = bandpower(x,fs,[0.5 4]);
pTheta  = bandpower(x,fs,[4 8]);
pAlpha  = bandpower(x,fs,[8 16]);
pBeta   = bandpower(x,fs,[16 32]);

% Relative delta power
pRelDelta = bandpower(x,fs,[0.5 4])./pTot;

% Relative theta power
pRelTheta = bandpower(x,fs,[4 8])./pTot;

% Relative alpha power
pRelAlpha = bandpower(x,fs,[8 16])./pTot;

% Relative beta power
pRelBeta = bandpower(x,fs,[16 32])./pTot;

% Spectral edge
edge = obw(x,fs,[],90);
% -------------------------------------------------------------------------


% Entropy measures
% -------------------------------------------------------------------------
shEn    = nan(1,M);
% pEn     = nan(1,M);
for i = 1:M
    % Shannon entropy
    shEn(i) = wentropy(x(:,i),'shannon');
end
% -------------------------------------------------------------------------


% Non-linear features
% -------------------------------------------------------------------------
% Hurst exponent
hurst   = nan(1,M);
for i = 1:M
    hurst(i) = hurst_exponent(x(:,i)');
end
ne = nonLinearEnergy(x);
% -------------------------------------------------------------------------

% Collect all features into suite
suite = [mpav; mu; sigma2; skew; kurt; mob; compl; p05; pDelta; pTheta; pAlpha; pBeta; pRelDelta; pRelTheta; pRelAlpha; pRelBeta; edge; shEn; hurst; ne];
suite = suite(:)';
end

function ne = nonLinearEnergy(x)
[N,M] = size(x);
x2 = x.^2;
xim = [zeros(1,M); x(1:N-1,:)];
xip = [x(2:N,:); zeros(1,M)];
nlo = x2 - xim.*xip;
ne = mean(nlo.*repmat(hamming(N),1,M));
end
