function SEM = getSEM(sigl, sigr, fs)
% This function calculates the slow eye movement feature for a signal 
% segmented in 2 s epochs.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Get cross-correlations
xcorrLo = getXCorr(sigl,sigr,fs,[0.5 6]);
xcorrHi = getXCorr(sigl,sigr,fs,[1 6]);

% SEM is difference between 1-6 and 0.5-6 band cross-correlations
SEM = xcorrHi - xcorrLo;

end