function yhat = predictUsingTree(X,thr)

% % Randomly sample half the instances
% nSubs = length(unique(id));
% rng(1);
% testSubs = randsample(nSubs,nSubs/2);
% testIdx = ismember(id,testSubs);

% Xdens = calcFeatureDensities(X(~testIdx,:),thr(1:15));
Xdens = calcFeatureDensities(X,thr(1:15));

% Classify each epoch
% hypno   = y(~testIdx);
% hypno   = hypno(1:57:end);
yhat    = nan(length(Xdens),1);
for k = 1:length(Xdens)
    
    % SW2T
    if Xdens(k,1) > thr(16)
        % SW3T
        if Xdens(k,2) > thr(17)
            yhat(k) = -3;
        else
            yhat(k) = -2;
        end
    else
        % ST
        if Xdens(k,3) > thr(18)
            % N1T
            if Xdens(k,4) > thr(19)
                yhat(k) = -1;
            else
                yhat(k) = 0;
            end
        else
            yhat(k) = 1;
        end
    end
end

yhat = smoothHypVirkk(yhat);

% % Calculate performance
% perf = calcPerf(confusionmat(y

% % Calculate misclassification error
% err = sum(hypno~=yhat)/numel(hypno);
end