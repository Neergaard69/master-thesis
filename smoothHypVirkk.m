function [smoothH,varargout] = smoothHypVirkk(H)
% This function takes an input hypnogram and applies smoothing rules as
% described in  Virkkala et al. "Automatic sleep stage classification using two-channel 
% electro-oculography" 2007.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

smoothH = H;
rules = zeros(1,4);

% Rule 1: Any REM epochs before the first N2 is replaced by W
firstN2 = find(H==-2,1,'first');
if any(H(1:firstN2) == 0)
    smoothH(find(H(1:firstN2) == 0)) = 1;
    rules(1) = 1;
end

% Rule 2: REM,N2,REM -> REM,REM,REM
for n = 1:length(H)-2
    if H(n) == 0 && H(n+1) == -2 && H(n+2) == 0
        smoothH(n+1) = 0;
        rules(2) = rules(2) + 1;
    end
end

% Rule 3: N2,N1,N2 -> N2,N2,N2
for n = 1:length(H)-2
    if H(n) == -2 && H(n+1) == -1 && H(n+2) == -2
        smoothH(n+1) = -2;
        rules(3) = rules(3) + 1;
    end
end

% Rule 4: W,REM,W -> W,W,W
for n = 1:length(H)-2
    if H(n) == 1 && H(n+1) == 0 && H(n+2) == 1
        smoothH(n+1) = 1;
        rules(4) = rules(4) + 1;
    end
end
smoothH = smoothH(:);
varargout{1} = rules;
end