function getNarcolepsyModelFeatures(pred_label)
% This function extracts features for use in narcolepsy model.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Set group and subject ID's
uniqueID = unique(ID);
nSubs = length(unique(ID));
psgFeatNames = getPSGfeatNames();
X = zeros(nSubs,20);
y = zeros(nSubs,1);

% Run subjects and extract features
for n = 1:nSubs
    y(n) = unique(G(ID==uniqueID(n)));
    h = Y(ID==uniqueID(n),1);
    X(n,:) = extractPSGfeatures(smoothHyp(h));
end

% Create test and training sets
rng(1)
cv = cvpartition(y,'HoldOut',1/3);
Xtrain = X(cv.training,:);
ytrain = y(cv.training);
Xtest = X(cv.test,:);
ytest = y(cv.test);
save(['diseaseFeats_all_' type '.mat'],'Xtrain','ytrain','Xtest','ytest');
end