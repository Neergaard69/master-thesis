% =========================================================================
% rf_train.M
%
% This script is used for training a random forest classifier based on
% features extracted using bd_featureExtraction.m
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

%% ========================================================================
%  INITIALIZATION
%  ------------------------------------------------------------------------

% Load data and set parameters
% -------------------------------------------------------------------------
clearvars; close all;
load('HC_norm.mat')
load('./../../Gateway/patient_data.mat')
[N,~] = size(X);
y = hypnogram(1:N);
C = length(unique(y));
stages = unique(y);
stageLabels = getStageNames();
featNames = getSleepFeatNames();
numLeaves = 1;
numTrees = 500;

%  ========================================================================


%% ========================================================================
%  RANDOM FOREST
%  ------------------------------------------------------------------------
% % Extract training data
Xtrain = X;
Ytrain = y;

% Train tree ensembles
pool = gcp;
tic
disp('---------------------------------------------------------------------------')
disp('| Training random forest models using all features...                                 |');
B = TreeBagger(numTrees,Xtrain,Ytrain,...
    'Method','classification',...
    'NVarToSample',sqrt(size(Xtrain,2)),...
    'MinLeaf',numLeaves,...
    'Options',statset('UseParallel',true),...
    'OOBPred','On');
oobErrorBaggedEnsemble = oobError(B);
disp('---------------------------------------------------------------------------')
disp('Done!')
toc
delete(pool);

% Find maximum accuracy
[~,optTrees] = max(1-oobErrorBaggedEnsemble);

save('rf_model.mat','B','optTrees');
