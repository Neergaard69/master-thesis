function names = getStageNames(flag)
% Returns stage names based on the value of the flag parameter.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

if nargin==0
    flag = 5;
end

switch flag
    case 2
        names = {'S','W'};
    case 3
        names = {'NREM','REM','W'};
    case 5
        names = {'N3','N2','N1','REM','WAKE'};
end

end