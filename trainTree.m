function invKappa = trainTree(X,y,thr)
% This function trains a decision tree using the rules given in 
% Virkkala et al. "Automatic sleep stage classification using two-channel 
% electro-oculography", 2007, and returns the negative Cohen's kappa for
% use in a minimization algorithm.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Calculate density features
Xdens = calcFeatureDensities(X,thr(1:15));

% Classify each epoch
yhat    = nan(length(Xdens),1);
for k = 1:length(Xdens)
    
    % SW2T
    if Xdens(k,1) > thr(16)
        % SW3T
        if Xdens(k,2) > thr(17)
            yhat(k) = -3;
        else
            yhat(k) = -2;
        end
    else
        % ST
        if Xdens(k,3) > thr(18)
            % N1T
            if Xdens(k,4) > thr(19)
                yhat(k) = -1;
            else
                yhat(k) = 0;
            end
        else
            yhat(k) = 1;
        end
    end
end


% Calculate Cohen's Kappa
hypno   = y(1:57:end);
invKappa = -1*cohens(confusionmat(hypno,smoothHypVirkk(yhat)));
% err = sum(hypno~=yhat)/numel(hypno);
end