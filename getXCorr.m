function xCorr = getXCorr(sig1,sig2,fs,frange)
% This function calculates correlation measures between two signals in a
% specified frequency band.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Create filters
Wn = (2/fs)*frange;
[z,p,k] = butter(4,Wn);
[sos,g] = zp2sos(z,p,k);

% Filter signals
sigs = filtfilt(sos,g,[sig1 sig2]);

% Segment signals
sig1_seg = segmentSignal(sigs(:,1),fs,2,0.5);
sig2_seg = segmentSignal(sigs(:,2),fs,2,0.5);

xCorr = nan(size(sig1_seg,2),1);
tic
parfor ii = 1:size(sig1_seg,2)
    R = corrcoef(sig1_seg(:,ii),sig2_seg(:,ii));
    xCorr(ii) = R(1,2);
end
toc

end