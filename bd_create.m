function bd_create(varargin)
%BD_CREATE Creates a H5 file of data for fast processing
% Use BD_CREATE() to create a H5 Matlab file for fast processing of large
% datasets.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

tic
% Check options structure
switch nargin
    case 0 % Default parameters from AASM standard
        fs          = 256;
        f           = [0.3 35];
        dataName   = 'data';
    case 1
        options     = varargin{1};
        fs          = options.fs;
        f           = options.f;
        dataName    = options.dataName;
end

% Include paths to folders
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Pseudoanonymized data - new'))
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Master\library'))

% Load subject file and set parameters
subjectFile         = 'subject_file.xlsx';
[~, ~, raw]         = xlsread(subjectFile);
subjects            = raw(2:end,1);
n_subjects          = size(subjects,1);
lightEpochsFile     = 'Light epochs.txt';
K_counter           = 0;
status              = [raw{2:end,3}]';

% Various parameters
labels              = [raw{:,3}];
hypLabels           = {'N3','N2','N1','R','W'};
unit                = '{\mu}V';

% Create filters for preprocessing
W = 2*f/fs;
[z,p,k] = butter(4,W);
[sos,g] = zp2sos(z,p,k);

% Create H5 file
if fileExist([dataName '.h5'])
    delete([dataName '.h5']);
end
h5create([dataName '.h5'],'/recs',[2 30*fs Inf],'ChunkSize',[2 30*fs 1]);
h5create([dataName '.h5'],'/hypnogram',[1 1 Inf],'ChunkSize',[1 1 1]);
h5create([dataName '.h5'],'/ID',[1 1 Inf],'ChunkSize',[1 1 1]);
h5create([dataName '.h5'],'/status',[1 1 Inf],'ChunkSize',[1 1 1]);

% Loop through subjects
for ii = 21:38%1:n_subjects
    % ---------------------------------------------------------------------
    % Data initialization
    % --------------------------------------------------------------------
    fprintf('Processing subject %d of %d...\n',ii,n_subjects);
    subjectName = subjects{ii,1};
    
    % Load hypnogram
    load([raw{ii+1,2} '/' subjectName '/vec_hypnogram.mat']);
    
    % Load left eye signal
    load([raw{ii+1,2} '/' subjectName '/eogl-a2.mat']);
    eogl_raw = eogla2;
    
    % Load right eye signal
    load([raw{ii+1,2} '/' subjectName '/eogr-a1.mat']);
    eogr_raw = eogra1;
    
    % ---------------------------------------------------------------------
    % Signal processing
    % ---------------------------------------------------------------------
    % Extract signals from lights off to lights on
    [eog_raw, hypno] = extractLightOffLightOn(subjectName,[eogl_raw, eogr_raw],hypnogram,fs,lightEpochsFile);
    
    if any(any(isnan(eog_raw)))
        fprintf('NaNs in subject %d!\n',ii);
        eog_raw(isnan(eog_raw)) = 0;
    end
    
    % Filtering
    eog = filtfilt(sos,g,eog_raw);
    
    % Set 0's to nans
    if any(any(eog==0))
        fprintf('NaNs in subject %d!\n',ii);
        eog(eog==0) = nan;
    end
    
    % Segment signal in 2x30*fsxN fashion
    N = length(eog);
    M = 30*fs; % Number of samples in each 30 s epoch
    K = N/M;
    eog_seg = reshape(eog',[2 M K]);
    
    % Set write parameters
    start = [1 1 K_counter+1];
    count_recs = size(eog_seg);
    count_hyp = [1 size(hypno')];
    
    % Set patient ID
    ID = ii*ones(count_hyp);
    
    % Set patient status
    stat = status(ii)*ones(count_hyp);
    
    % Write signals in H5 file
    h5write([dataName '.h5'],'/recs',eog_seg,start,count_recs);
    h5write([dataName '.h5'],'/hypnogram',reshape(hypno',count_hyp),start,count_hyp);
    h5write([dataName '.h5'],'/ID',ID,start,count_hyp);
    h5write([dataName '.h5'],'/status',stat,start,count_hyp);
    
    % Update K_counter
    K_counter = K_counter + K;
end

% Write attribute containing length of dataset
h5writeatt([dataName '.h5'],'/','N',K_counter);

toc
end

function tf = fileExist(fileName)
currentDir = dir;
tf = any(strcmp({currentDir.name},fileName));
end
