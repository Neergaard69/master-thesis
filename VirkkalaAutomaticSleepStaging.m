%% SLEEP STAGE CLASSIFICATION BASED ON POWER AND CORRELATION MEASURES
%
%  This is an implementation of the EOG sleep classifier described by
%  Virkkala et al. "Automatic sleep stage classification using two-channel 
%  electro-oculography" 2007.
%
%  Implemented by Alexander Neergaard Olesen, 
%  Technical University of Denmark, 2016
%  ========================================================================

%% Initalization
%  ========================================================================
% -------------------------------------------------------------------------
clearvars

% Include paths to folders
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Pseudoanonymized data\Training'))
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Master\library'))

% Load subject file
subjectFile         = 'subject_file.xlsx';
[~, ~, raw]         = xlsread(subjectFile);
subjects            = raw(2:end,1);
% n_subjects          = size(subjects,1);
n_subjects          = 20;

% Specify parameters for feature extraction
featureMatrix       = nan(1200000,7);
ID                  = nan(1200000,1);
hypTotal            = nan(1200000,1);
labels              = [raw{:,3}];
lightEpochsFile     = 'Light epochs.txt';
hypLabels           = {'N3','N2','N1','R','W'};
unit                = '{\mu}V';
fs                  = 256;

% Create filters for preprocessing
Wn = (2/fs)*[0.3 35];
[z,p,k] = butter(4,Wn);
[sigSOS,sigG] = zp2sos(z,p,k);
% -------------------------------------------------------------------------

%% Loop through subjects
%  ========================================================================
for i = 1:n_subjects
    
    % -------------------------------------------------------------------------
    % Data initialization
    % -------------------------------------------------------------------------
    fprintf('Processing subject %d of %d...\n',i,n_subjects);
    subjectName = subjects{i,1};
    
    % Load hypnogram
    load([raw{i+1,2} '/' subjectName '/vec_hypnogram.mat']);
    
    % Load left eye signal
    load([raw{i+1,2} '/' subjectName '/eogl-a2.mat']);
    eogl_raw = eogla2;
    
    % Load right eye signal
    load([raw{i+1,2} '/' subjectName '/eogr-a1.mat']);
    eogr_raw = eogra1;
    
    
    % -------------------------------------------------------------------------
    % Preprocessing
    % -------------------------------------------------------------------------
    
    % Extract signals from lights off to lights on
    [eog_raw, hypno] = extractLightOffLightOn(subjectName,[eogl_raw eogr_raw],hypnogram,fs,lightEpochsFile);
    
    % Filtering
    idxNan = isnan(eog_raw);
    eog_raw(idxNan) = 0;
    eog = filtfilt(sigSOS,sigG,eog_raw);
    eog(idxNan) = nan;
    
    
    % -------------------------------------------------------------------------
    % Feature extraction
    % -------------------------------------------------------------------------
    % Get alpha power from left channel
    alpha = getAlpha(eog(:,1),fs);
    
    % Get beta power from difference signal
    beta = getBeta(eog(:,1)-eog(:,2),fs);
    
    % Get SEM feature
    SEM = getSEM(eog(:,1),eog(:,2),fs);
    
    % Get correlation features
    xCorr = [getXCorr(eog(:,1),eog(:,2),fs,[0.5 6]) getXCorr(eog(:,1),eog(:,2),fs,[1.5 6])];
    
    % Amplitude features
    amps = [getP2PDiff(eog(:,1)-eog(:,2),fs,[0.5 6]) getP2PDiff(eog(:,1)-eog(:,2),fs,[1.5 6])];
    
    % Number of mini-epochs
    K = length(hypno)*57;
    
    % Put all features into feature matrix
    currentIdx = find(isnan(ID),1,'first');
    featureMatrix(currentIdx:currentIdx+K-1,:) = [alpha beta SEM xCorr amps];
    
    % Update patId and hypTotal variables
    ID(currentIdx:currentIdx+K-1) = i;
    hypTotal(currentIdx:currentIdx+K-1) = reshape(repmat(hypno',57,1),[],1);
end

disp('Done.')

% Remove Nan values
nanIdx = find(isnan(ID),1,'first');
featureMatrix(nanIdx:end,:) = [];
ID(nanIdx:end) = [];
hypTotal(nanIdx:end) = [];
save('virkkala.mat','featureMatrix','hypTotal','ID')



%% Classification
% ========================================================================

% -------------------------------------------------------------------------
% TEST 1:

nRepetitions = 10;
load('virkkala.mat');
bestKappa = zeros(nRepetitions,1);
bestThr = zeros(nRepetitions,19);

for ii = 1:nRepetitions
    
% Optimize thresholds
% ========================================================================

% Get train subjects
nSubs = length(unique(ID));
rng(ii);
testSubs = randsample(nSubs,nSubs/2);
testIdx = ismember(ID,testSubs);

% Initialize search
thr0 = [0.2 12 5 0.07 -0.3 31 8 0.14 9.5 10 -0.65 0 0.2 8.5 7 0.25 0.28 0.48 0.47];
opts = optimset('Display','iter');

% Repeat optimization
for k = 1:10
    [thr0,invKappa] = fminsearch(@(thr)trainTree(featureMatrix(~testIdx,:),hypTotal(~testIdx),thr),thr0.*randsample([0.9 1.1],19,'true'),opts);
    if invKappa < bestKappa(ii)
        bestThr(ii,:) = thr0;
        bestKappa(ii) = invKappa
    end
end

% Validate on test subjects
% ------------------------------------------------------------------------

% Run test
for n = 1:length(testSubs)
    % Test model
    t1results(n).yhat = predictUsingTree(featureMatrix(ID == testSubs(n),:),bestThr(ii,:));
    ytest = hypTotal(ID == testSubs(n));
    ytest = ytest(1:57:end);
    t1results(n).confMat = confusionmat(ytest,t1results(n).yhat);
    t1results(n).perf = calcPerf(t1results(n).confMat);
end

    % Add confusion matrices and calculate final performance
    confMat = sum(reshape([t1results.confMat],[5 5 length(testSubs)]),3);
    perft1(ii) = calcPerf(confMat);

end
avgAcc = 100*[mean([perft1(:).Accuracy]) std([perft1(:).Accuracy])]
avgKappa = [mean([perft1(:).kappa]) std([perft1(:).kappa])]
avgSens = 100*[mean(reshape([perft1(:).Sensitivity],5,nRepetitions),2) std(reshape([perft1(:).Sensitivity],5,nRepetitions),0,2)]

% -------------------------------------------------------------------------