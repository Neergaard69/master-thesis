%% SLEEP STAGE CLASSIFICATION
%  BASED ON MSE/LDA CLASSIFICATION
%
%  This is an implementation of the EOG sleep classifier described by
%  Liang et al. "Development of an EOG-Based Automatic Sleep-Monitoring Eye
%  mask" 2015.
%  ========================================================================

%% Initalization
%  ========================================================================
% -------------------------------------------------------------------------
clearvars

% Include paths to folders
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Pseudoanonymized data\Training'))
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Master\library'))

% Load subject file
subjectFile         = 'subject_file.xlsx';
[~, ~, raw]         = xlsread(subjectFile);
subjects            = raw(2:end,1);
n_subjects          = size(subjects,1);

% Specify parameters for feature extraction
ARorder             = 8;
tau                 = 1:13;
featureMatrix       = nan(20000,length(tau)+ARorder);
patID               = nan(20000,1);
hypTotal            = nan(20000,1);
labels              = [raw{:,3}];
lightEpochsFile     = 'Light epochs.txt';
hypLabels           = {'N3','N2','N1','R','W'};
unit                = '{\mu}V';
fs                  = 256;

% Create filters for preprocessing
Wn = (2/fs)*[0.3 35];
[z,p,k] = butter(4,Wn);
[sos,g] = zp2sos(z,p,k);

% Create filters for AR feature extraction
WnAR = (2/fs)*[4 8];
[z,p,k] = butter(4,WnAR);
[ARsos,ARg] = zp2sos(z,p,k);
% -------------------------------------------------------------------------

%% Loop through subjects
%  ========================================================================
for i = 1:20
    
    % ---------------------------------------------------------------------
    % Data initialization
    % ---------------------------------------------------------------------
    fprintf('Processing subject %d of %d...\n',i,20);
    subjectName = subjects{i,1};
    
    % Load hypnogram
    load([raw{i+1,2} '/' subjectName '/vec_hypnogram.mat']);
    
    % Load left eye signal
    load([raw{i+1,2} '/' subjectName '/eogl-a2.mat']);
    eogl_raw = eogla2;
    
    % Load right eye signal
    load([raw{i+1,2} '/' subjectName '/eogr-a1.mat']);
    eogr_raw = eogra1;
    
    % ---------------------------------------------------------------------
    % Preprocessing
    % ---------------------------------------------------------------------
    
    % Extract signals from lights off to lights on
    [eog_raw_lolo, hypno] = extractLightOffLightOn(subjectName,[eogl_raw eogr_raw],hypnogram,fs,lightEpochsFile);
    
%     % Normalize using rms
%     eog_raw_lolo = eog_raw_lolo./repmat(rms(eog_raw_lolo),length(eog_raw_lolo),1);
    
    % Create differential signal
    eog_raw_lolo = eog_raw_lolo(:,1) - eog_raw_lolo(:,2);
    
    % Filtering
    idxNan = isnan(eog_raw_lolo);
    eog_raw_lolo(idxNan) = 0;
    eog = filtfilt(sos,g,eog_raw_lolo);
    AReog = filtfilt(ARsos,ARg,eog_raw_lolo);
%     eog(idxNan) = nan;
%     AReog(idxNan) = nan;
    
    % ---------------------------------------------------------------------
    % Feature extraction
    % ---------------------------------------------------------------------
    N = length(eog);
    M = 30*fs; % Number of samples in each 30 s epoch
    K = N/M;
    counter = 0;
    featureBuffer = nan(K,length(tau)+ARorder);
    ARfeatsIdx = 1:ARorder;
    MSEfeatsIdx = (1:length(tau))+ARorder;
    
    disp('Running feature extraction:');
    signal = reshape(eog,M,K);
    
    % -------------------------------------------------------------------------
    disp('Extracting AR features...');
    ARsignal = reshape(AReog,M,K);
    a = arburg(ARsignal,ARorder);
    disp('Extracted AR features succesfully.');
    
    % Put AR features into buffer
    featureBuffer(:,ARfeatsIdx) = a(:,2:end);
    
    % -------------------------------------------------------------------------
    disp('Extracting MSE features...');
    
    % Get r value from entire eog
    r = 0.2*std(signal);
    
    % Initialize for parallel
    pool = gcp;
    parfor_progress(K)
    tic
    mseBuffer = nan(K,13);
    
    parfor k = 1:K
        
        % Extract MSE features
%         mseBuffer(k,:) = msentropy(signal(:,k),[],[],[],[],[],[],[],13,[],[]);
        mseBuffer(k,:) = msent(signal(:,k),r(k),tau);
        
        % Update parfor progress
        parfor_progress;
    end
    
    % Delete pool object
    parfor_progress(0);
    disp('Extracted MSE features successfully.');
    toc
%     delete(pool);
    
    % Put MSE features in buffer
    featureBuffer(:,MSEfeatsIdx) = mseBuffer;
    
    % Put all features into feature matrix
    currentIdx = find(isnan(patID),1,'first');
    featureMatrix(currentIdx:currentIdx+K-1,:) = featureBuffer;
    
    % Update patId and hypTotal variables
    patID(currentIdx:currentIdx+K-1) = i;
    hypTotal(currentIdx:currentIdx+K-1) = hypno;
    
end

% Delete pool object
delete(pool);

% Remove Nan values
nanIdx = find(isnan(patID),1,'first');
featureMatrix(nanIdx:end,:) = [];
patID(nanIdx:end) = [];
hypTotal(nanIdx:end) = [];
% save('liang_norm.mat','featureMatrix','hypTotal','patID')


%% Linear Discriminant Analysis
%  ========================================================================
% clearvars
% load('liang_norm.mat')

% TEST 1:
% -------------------------------------------------------------------------
nRepetitions = 100;

for ii = 1:nRepetitions
    % Get random sampling of 20 subjects
    test1Idx = randsample(20,16);
    
    % Get train and test idx by random sampling
    testIdx = randsample(test1Idx,8);
    Xtrain  = featureMatrix(ismember(patID,setdiff(test1Idx,testIdx)),:);
    ytrain  = hypTotal(ismember(patID,setdiff(test1Idx,testIdx)));
    Xtest   = featureMatrix(ismember(patID,testIdx),:);
    ytest   = hypTotal(ismember(patID,testIdx));
    
    % Train linear discriminat
    t1mdl = fitcdiscr(Xtrain,ytrain,'DiscrimType','linear','classNames',-3:1);
    
    for n = 1:8
        % Test model
        t1results(n).yhat = smoothHyp(predict(t1mdl,featureMatrix(patID == testIdx(n),:)));
        t1results(n).confMat = confusionmat(hypTotal(patID == testIdx(n)),t1results(n).yhat);
        t1results(n).perf = calcPerf(t1results(n).confMat);
    end
    
    % Add confusion matrices and calculate final performance
    confMat = sum(reshape([t1results.confMat],[5 5 8]),3);
    perf(ii) = calcPerf(confMat);
end
avgAcc = 100*[mean([perf(:).Accuracy]) std([perf(:).Accuracy])]
avgKappa = [mean([perf(:).kappa]) std([perf(:).kappa])]
avgSens = 100*[mean(reshape([perf(:).Sensitivity],5,nRepetitions),2) std(reshape([perf(:).Sensitivity],5,nRepetitions),0,2)]


% TEST 2:
% -------------------------------------------------------------------------
nRepetitions = 100;
nFoldCV = 10;
test2Idx = ismember(patID,1:20);

for ii = 1:nRepetitions
    % Train and crossvalidate
    cvFolds = cvpartition(hypTotal(test2Idx),'KFold',nFoldCV);
    cvModel = fitcdiscr(featureMatrix(test2Idx,:),hypTotal(test2Idx),'DiscrimType','linear','classNames',-3:1,'CVPartition',cvFolds);
    
    cvyhat = kfoldPredict(cvModel);
    for n = 1:nFoldCV
        % Test cross-validated model
        t2results(n).yhat = smoothHyp(cvyhat(test2Idx));
        t2results(n).confMat = confusionmat(hypTotal(test2Idx),t2results(n).yhat);
        t2results(n).perf = calcPerf(t2results(n).confMat);
    end
    
    % Add confusion matrices and calculate final performance
    confMat = sum(reshape([t2results.confMat],[5 5 nFoldCV]),3);
    perft2(ii) = calcPerf(confMat);
end
avgAcc = 100*[mean([perft2(:).Accuracy]) std([perft2(:).Accuracy])]
avgKappa = [mean([perft2(:).kappa]) std([perft2(:).kappa])]
avgSens = 100*[mean(reshape([perft2(:).Sensitivity],5,100),2) std(reshape([perft2(:).Sensitivity],5,100),0,2)]