function bd_createEMD(dataName)
%BD_CREATEEMD Runs CEEMDAN and saves in H5 data for fast processing.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016
tic

% Include paths to folders
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Pseudoanonymized data - new'))
addpath(genpath('\\10.230.149.201\AlexanderNeergaardOlesen2015\Master\library'))
    
% Extract number of epochs
N = h5readatt(dataName,'/','N');

% Sampling frequency and length of epoch
fs = 256;
L = 30*fs;

% Create H5 dataset
h5create(dataName,'/imfl',[10 L Inf],'ChunkSize',[10 L 1]);
h5create(dataName,'/imfr',[10 L Inf],'ChunkSize',[10 L 1]);


% Loop through subjects

fprintf('\nEpoch no: ')

for ii = 1:N
    % Display current epoch
    if ii > 1
        for j = 1:9
            fprintf('\b');
        end
        for j = 0:log10(ii-1)
            fprintf('\b');
        end
    end
    
    fprintf('%d of %d',ii,N);
    
    % Load signal
    eog = h5read(dataName,'/recs',[1 1 ii],[2 L 1])';
    
    % Extract IMF's
    IMF = nan(10,length(eog));
    repeat = true;
    while repeat
        try
            modes = ceemdanc(eog(:,1),0.2,100,50,1);
            if size(modes,1) >= 10
                repeat = false;
            end
        catch
            continue
        end
    end
    IMF = modes(1:10,:);
    
    % Write to H5 file
    h5write(dataName,'/imfl',IMF,[1 1 ii],[10 L 1]);
    
    % Extract IMF's
    IMF = nan(10,length(eog));
    repeat = true;
    while repeat
        try
            modes = ceemdanc(eog(:,2),0.2,100,50,1);
            if size(modes,1) >= 10
                repeat = false;
            end
        catch
            continue
        end
    end
    IMF = modes(1:10,:);
    
    % Write to H5 file
    h5write(dataName,'/imfr',IMF,[1 1 ii],[10 L 1]);
end

toc
end

function tf = fileExist(fileName)
currentDir = dir;
tf = any(strcmp({currentDir.name},fileName));
end