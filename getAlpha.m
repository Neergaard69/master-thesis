function alpha = getAlpha(sig,fs)
% This function calculates alpha power in a signal segmented in 2 s epochs.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Segment signal
sig_seg = segmentSignal(sig,fs,2,0.5);

% Get alpha power for all
alpha = bandpower(sig_seg,fs,[8 12])';

end