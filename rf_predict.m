% =========================================================================
% rf_predict.M
%
% This script is used for predicting new labels given a set of
% observations using a random forest classifier.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016


% Load data
load('datamatrix.mat');
load('rf_model.mat');

% Predict new labels
Y = cellfun(@str2num,smoothHyp(predict(B,X,'Trees',1:optTrees)));

save('pred_label','Y');