function sig_seg = segmentSignal(sig,fs,seg_sz,stp_sz)
% This function segments a signal sig sampled at fs in segments of size
% seg_sz with a step size of stp_sz
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

sig = sig(:);

% Number of samples in 30 s epoch
L = 30*fs;

% Get number of epochs
N = length(sig)/L;

% Mini-epoch parameters
wnd_sz = seg_sz;
wndfs = wnd_sz*fs;
stpfs = stp_sz*fs;
ws_r = wnd_sz/stp_sz;
wnd_epch = 2*(30-1)-1;

% Segment signal
sig_seg = zeros(wndfs,N*wnd_epch);
for ii = 1:N
    tmp = sig((ii-1)*L+1:ii*L);
    for jj = 1:ws_r
        if jj == 1
            sig_seg(:,(jj:ws_r:wnd_epch)+(ii-1)*wnd_epch) = reshape(tmp,wndfs,L/wndfs);
        else
            sig_seg(:,(jj:ws_r:wnd_epch)+(ii-1)*wnd_epch) = reshape(tmp((jj-1)*stpfs+1:end-(ws_r-jj+1)*stpfs),wndfs,L/wndfs-1);
        end
    end
end

end