function amps = getP2PDiff(sig,fs,frange)
% This function calculates the peak to peak difference of a signal in a
% specified frequency range.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Create filters
Wn = (2/fs)*frange;
[z,p,k] = butter(4,Wn);
[sos,g] = zp2sos(z,p,k);

% Filter signals
sigfilt = filtfilt(sos,g,sig);

% Segment signal
sig_seg = segmentSignal(sigfilt,fs,2,0.5);

% Get P2P diff
amps = peak2peak(sig_seg)';

end