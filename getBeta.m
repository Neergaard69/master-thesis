function beta = getBeta(sig,fs)
% This function calculates beta power in a signal segmented in 2 s epochs.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Segment signal
sig_seg = segmentSignal(sig,fs,2,0.5);

% Get alpha power for all
beta = bandpower(sig_seg,fs,[18 30])';

end