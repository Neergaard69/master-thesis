function featDens = calcFeatureDensities(featureMatrix,thr)

% Distribute features
alpha   = featureMatrix(:,1);
beta    = featureMatrix(:,2);
SEM     = featureMatrix(:,3);
xCorr   = featureMatrix(:,4:5);
amps    = featureMatrix(:,6:7);

% Number of mini-epochs per epoch
nMini = 57; % 2*(30-1)-1

% Get number of epochs
K = length(alpha)/nMini;

% Create
featDens = nan(K,4);

% Loop through all epochs
for k = 1:K
    idx = (k-1)*nMini+1:k*nMini;
    
    % Rule 1
    featDens(k,1) = sum(xCorr(idx,1) > thr(1) & amps(idx,1) > thr(2) & beta(idx) < thr(3) & SEM(idx) < thr(4));
    
    % Rule 2
    featDens(k,2) = sum(xCorr(idx,1) > thr(5) & amps(idx,1) > thr(6) & beta(idx) < thr(7) & SEM(idx) < thr(8));
    
    % Rule 3
    featDens(k,3) = sum(beta(idx) < thr(9) & alpha(idx) < thr(10) & ( xCorr(idx,2) > thr(11) | xCorr(idx,2) < thr(12)));
    
    % Rule 4
    featDens(k,4) = sum(xCorr(idx,2) > thr(13) & amps(idx,2) > thr(14) & beta(idx) < thr(15));
end

featDens = featDens/nMini;

end