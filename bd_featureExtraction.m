function X = bd_featureExtraction(inputFile,S)
% This function takes an input H5 file and subject group S and return the
% corresponding feature matrix.
%
% //Alexander Neergaard Olesen
% //Technical University of Denmark, 2016

% Check for H5 file
if ~(exist(inputFile,'file') == 2)
    error('Please run bd_create() to create H5 file!');
end

% Extract number of epochs
N = h5readatt(inputFile,'/','N');

% Extract patient info
patient_data = load('\\10.230.149.201\AlexanderNeergaardOlesen2015\Master\emd_analysis\patient_data.mat');
ids = patient_data.id(patient_data.group == S);
ids(1:end-N) = [];
uniqueIDs = unique(ids);

% Sampling frequency
fs = 256;
L = 30*fs;

% Number of features
M = 201;

% Create feature matrix
X = nan(N,M-1);
C = nan(N,1);
rmsVal = nan(length(uniqueIDs),2);

% Create pool object
p = gcp;
parfor_progress(length(uniqueIDs));
tic
parfor ii = 1:length(uniqueIDs)
    
    start = find(ids == uniqueIDs(ii),1,'first');
    stop = find(ids == uniqueIDs(ii),1,'last');
    
    % Load eog
    eog = reshape(h5read(inputFile,'/recs',[1 1 start],[2 L stop-start+1]),2,[],1)';
    
    % Calculate RMS value in order to normalize IMFs
    rmsVal(ii,:) = rms(eog);
    
    % Update progress
    parfor_progress;
    
end
parfor_progress(0);


parfor_progress(N);
parfor ii = 1:N
    
    % Load IMF's
    imf = h5read(inputFile,'/imfl',[1 1 ii],[10 L 1])/rmsVal(uniqueIDs==ids(ii),1);
    
    % Extract feature suite
    X(ii,:) = extractFeatureSuite(imf',fs);
    
    % Extract corr coeff
    R = corrcoef(sum(h5read(inputFile,'/imfr',[6 1 ii],[3 L 1])/rmsVal(uniqueIDs==ids(ii),2))',sum(imf(6:9,:))');
    C(ii) = R(1,2);
    
    % Update progress bar
    parfor_progress;
end

% Delete pool object
parfor_progress(0);
toc
delete(p);

X = [X C];